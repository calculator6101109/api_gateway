package config


import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/spf13/cast"
	"os"
)


type Config struct {
	ServiceName string

	HTTPPort string

	CalculatorGRPCServiceHost string
	CalculatorGRPCServicePort string
}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("error!!!", err)
	}

	cfg := Config{}

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "store"))

	cfg.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))

	cfg.CalculatorGRPCServiceHost = cast.ToString(getOrReturnDefault("CALCULATOR_GRPC_SERVICE_HOST", "localhost"))
	cfg.CalculatorGRPCServicePort = cast.ToString(getOrReturnDefault("CALCULATOR_GRPC_SERVICE_PORT", ":8001"))

	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	value := os.Getenv(key)
	if value != "" {
		return value
	}

	return defaultValue
}
