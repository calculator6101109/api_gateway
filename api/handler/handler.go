package handler


import (
	"api_gateway/config"
	pb "api_gateway/protos"
)

type Handler struct {
	calculatorService pb.CalculatorServicesClient
}


func New(cfg config.Config, calculatorService pb.CalculatorServicesClient) Handler {
	return Handler{
		calculatorService: calculatorService,
	}
}
