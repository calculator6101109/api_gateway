package handler

import (
	"api_gateway/api/models"
	pb "api_gateway/protos"
	"context"
	"log"

	"github.com/gin-gonic/gin"
)

func (h *Handler) Addition(c *gin.Context) {
	request := models.CreateNumber{}

	if err := c.ShouldBind(&request); err != nil {
		panic(err)
	}

	resp, err := h.calculatorService.Calculate(context.Background(), &pb.CreateNumber{
		FirstNumber:  request.FirstNumber,
		SecondNumber: request.SecondNumber,
	})
	if err != nil {
		log.Println("error while addition in handler...")
		return
	}

	c.JSON(200, resp)
}
