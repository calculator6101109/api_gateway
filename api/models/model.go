package models

type CreateNumber struct {
	FirstNumber  int32 `json:"first_number"`
	SecondNumber int32 `json:"second_number"`
}

type Result struct {
	Result int32 `json:"result"`
}
